package com.sic.resource;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sic.dto.EmailRequest;
import com.sic.dto.EmailResponse;
import com.sic.dto.EmailSimplesRequest;
import com.sic.service.EmailService;

@RestController
@CrossOrigin
@RequestMapping("/email")
public class EmailResource {

	@Autowired
	private EmailService emailService;

	@RequestMapping(method = RequestMethod.POST, path = "/cotacao")
	public ResponseEntity<EmailResponse> enviarEmailCotacao(@RequestBody EmailRequest request) {
		if(Objects.nonNull(request) && Objects.nonNull(request.getDestinatarios()) && request.getDestinatarios().size() > 0) {
			return ResponseEntity.ok(this.emailService.insertEmailParaEnvio(request));
		}else {
			return ResponseEntity.noContent().build();
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/envio")
	public ResponseEntity<EmailResponse> enviarEmail(@RequestBody EmailSimplesRequest request) {
		if(Objects.nonNull(request)) {
			EmailResponse email = this.emailService.envioEmail(request);
			return ResponseEntity.ok(email);
		}else {
			return ResponseEntity.noContent().build();
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/lista")
	public ResponseEntity<EmailResponse> enviarEmail(@RequestBody List<EmailSimplesRequest> request) {
		if(Objects.nonNull(request) && !request.isEmpty()) {
			EmailResponse email = null;
			try {
				email = this.emailService.envioEmailLista(request);
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return ResponseEntity.ok(email);
		}else {
			return ResponseEntity.noContent().build();
		}
	}
}
