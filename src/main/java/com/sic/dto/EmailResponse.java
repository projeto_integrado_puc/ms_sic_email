package com.sic.dto;

import java.io.Serializable;

public class EmailResponse implements Serializable{
	
	private static final long serialVersionUID = -4031734341697261270L;
	
    private String mensagemSistema;
    private String mensagemDesenvolvimento;
    
	public String getMensagemSistema() {
		return mensagemSistema;
	}
	public void setMensagemSistema(String mensagemSistema) {
		this.mensagemSistema = mensagemSistema;
	}
	public String getMensagemDesenvolvimento() {
		return mensagemDesenvolvimento;
	}
	public void setMensagemDesenvolvimento(String mensagemDesenvolvimento) {
		this.mensagemDesenvolvimento = mensagemDesenvolvimento;
	}

}
