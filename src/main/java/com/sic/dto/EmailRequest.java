package com.sic.dto;

import java.io.Serializable;
import java.util.List;

public class EmailRequest implements Serializable{
	
	private static final long serialVersionUID = -4031734341697261270L;
	
    List<Destinatario> destinatarios;
    private String assunto;
    private String texto;
    
	public List<Destinatario> getDestinatarios() {
		return destinatarios;
	}
	public void setDestinatarios(List<Destinatario> destinatarios) {
		this.destinatarios = destinatarios;
	}
	public String getAssunto() {
		return assunto;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
}
