package com.sic.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.sic.service.ScheduleEmail;

@Service
public class EmailJob {

	@Autowired
	private ScheduleEmail scheduleEmail;

	@Scheduled(cron = "0 00/2 * * * *")
	public void mainSchedulle() {
		this.scheduleEmail.enviaEmail();
	}
}