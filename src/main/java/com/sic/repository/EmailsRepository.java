package com.sic.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sic.enums.StatusEmail;
import com.sic.model.Email;

public interface EmailsRepository extends JpaRepository<Email, Long> {
	
	@Query(value = " select e from Email e where e.status = :status")
	List<Email> findStatus(@Param("status") StatusEmail status, Pageable pageable);
	
	@Modifying
	@Transactional
	@Query("update Email set status = :status, horaEnvio = :horaEnvio where id IN (:ids)")
	void setStatusHoraEnvioById(@Param("status") StatusEmail status, @Param("horaEnvio") String horaEnvio, @Param("ids") List<Long> ids);
}