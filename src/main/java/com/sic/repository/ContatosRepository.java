package com.sic.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sic.enums.StatusEmail;
import com.sic.model.Contato;

public interface ContatosRepository extends JpaRepository<Contato, Long> {
	
	@Modifying
	@Transactional
	@Query("update Contato c set c.status = :status where c.id = :id")
	void setStatusById(@Param("status") StatusEmail status, @Param("id") Long id);
}