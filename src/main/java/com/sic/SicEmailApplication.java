package com.sic;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class SicEmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(SicEmailApplication.class, args);
	}
	
	@Bean(name = "beanEnvioEmail")
	@Qualifier("jobEnvio")
	public ThreadPoolTaskExecutor taskExecutorElektro() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(1);
		executor.setMaxPoolSize(1);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("shcedule-");
		executor.initialize();
		return executor;
	}
}
