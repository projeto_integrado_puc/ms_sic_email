package com.sic.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.sic.enums.StatusEmail;
import com.sic.model.Contato;
import com.sic.model.Email;
import com.sic.repository.ContatosRepository;
import com.sic.repository.EmailsRepository;
import com.sic.service.util.ContatoUtil;

@Service
public class ScheduleEmail{

	@Autowired
	@Qualifier("jobEnvio")
	private ThreadPoolTaskExecutor task;
	
	@Autowired
	private ContatosRepository contatoRepository;
	
	@Autowired
	private EmailsRepository emailRepository;
	
	@Autowired
	private JavaMailSender emailSender;
	
	@Value("${spring.mail.username}")
	private String fromEmail;

	Logger log = LoggerFactory.getLogger(this.getClass().getName());

	public void enviaEmail() {
		if (this.task.getActiveCount() == 0) {
			List<Email> emails = this.emailRepository.findStatus(StatusEmail.NAO_ENVIADO, PageRequest.of(0, 10));
			if (Objects.nonNull(emails) && !emails.isEmpty()) {
				log.info("Dados BD: Encontrado o total de " + emails.size()  + " registros de e-mails não enviados.");
				createTaskServico(emails);
			} else {
				log.info("Dados BD: Não foram encontrados e-mails a serem enviados.");
			}
		} else {
			log.info("Threads sendo processadas.");
		}
	}

	private void createTaskServico(List<Email> emails) {
		log.info("Inicio criação das Tasks");
		List<Long> idsEmail = new ArrayList<>();
		Date dataAtual = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String dataHoraEnvio = dateFormat.format(dataAtual);
		for (Email email : emails) {
			for(Contato contato : email.getContatos()) {
				this.task.execute(new Runnable() {
					@Override
					public void run() {
						processaTaskEnvioEmail(email.getAssunto(), email.getCorpoEmail(), contato);
					}
				});
			}
			idsEmail.add(email.getId());
		}
		if(!idsEmail.isEmpty()) {
			log.info("Atualiza status do e-mail");
			this.emailRepository.setStatusHoraEnvioById(StatusEmail.ENVIADO, dataHoraEnvio, idsEmail);
		}
	}
	
	@Async("beanEnvioEmail")
	public void processaTaskEnvioEmail(String assunto, String corpoEmail, Contato contato) {
		log.info("Envio de e-mail");
		
		try {
			SimpleMailMessage message = montaEmail(assunto, corpoEmail, contato.getEmail(), contato.getChaveValor());
			emailSender.send(message);
			this.contatoRepository.setStatusById(StatusEmail.ENVIADO, contato.getId());
		} catch (MailException e) {
			System.out.println(e.getMessage());
			this.contatoRepository.setStatusById(StatusEmail.ERROR, contato.getId());
		}

	}
	
	private SimpleMailMessage montaEmail(String assunto, String corpoEmail, String destinatario, String parametros) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(this.fromEmail);
		message.setTo(destinatario);
		message.setSubject(assunto);
		message.setText(ContatoUtil.insertParamTexto(corpoEmail, parametros));
		return message;
	}
}
