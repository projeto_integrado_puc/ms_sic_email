package com.sic.service.util;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import org.apache.logging.log4j.util.Strings;

import com.sic.dto.Parametro;

public class ContatoUtil implements Serializable {

	private static final long serialVersionUID = -4511994062396038478L;

	public static String insertParamTexto(String corpoEmail, String parametros) {
		String retorno = new String(corpoEmail);
		String[] arrayChaveValor = (Strings.isNotBlank(parametros)) ? parametros.split(";") : null;
		if(Objects.nonNull(arrayChaveValor) && arrayChaveValor.length > 0) {
			for(String p : arrayChaveValor) {
				String[] param = p.split(":");
				if(Objects.nonNull(param) && param.length == 2) {
					retorno = retorno.replace(new String(param[0]), new String(param[1]));
				}
			}
		}
		return retorno;
	}
	
	public static String loadParametros(List<Parametro> parametros) {
		StringBuilder sb = new StringBuilder();
		for(Parametro p : parametros) {
			if(sb.isEmpty()) {
				sb.append(p.getChave()).append(":").append(p.getValor());
			}else {
				sb.append(";").append(p.getChave()).append(":").append(p.getValor());
			}
		}
		return sb.toString();
	}
}
