package com.sic.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.sic.dto.Destinatario;
import com.sic.dto.EmailRequest;
import com.sic.dto.EmailResponse;
import com.sic.dto.EmailSimplesRequest;
import com.sic.enums.StatusEmail;
import com.sic.model.Contato;
import com.sic.model.Email;
import com.sic.repository.ContatosRepository;
import com.sic.repository.EmailsRepository;
import com.sic.service.util.ContatoUtil;

@Service
public class EmailService {

	@Autowired
	private ContatosRepository contatoRepository;

	@Autowired
	private EmailsRepository emailRepository;
	
	@Value("${spring.mail.username}")
	private String fromEmail;
	
	@Autowired
	private JavaMailSender emailSender;
	
	public EmailResponse envioEmailLista(List<EmailSimplesRequest> requests) {
		EmailResponse response = new EmailResponse();
		
		for(EmailSimplesRequest email : requests) {
			try {
				envioEmail(email);
			}catch (Exception e) {
				System.out.println("Falha ao enviar o e-mail");
			}
		}
		
		response.setMensagemSistema("E-mail inserido.");
		
		return response;
	}
	
	public EmailResponse envioEmail(EmailSimplesRequest request) {		
		EmailResponse response = new EmailResponse();

		Contato contato = new Contato();
		contato.setEmail(request.getDestinatario());
		contato.setStatus(StatusEmail.NAO_ENVIADO);
		contato.setChaveValor("email" + request.getDestinatario());
		List<Contato> listContatos = Arrays.asList(contato);
		
		listContatos = insertContatos(listContatos);

		Email email = new Email();
		email.setAssunto(request.getAssunto());
		email.setContatos(listContatos);
		email.setCorpoEmail(request.getTexto());
		email.setStatus(StatusEmail.NAO_ENVIADO);

		this.emailRepository.save(email);

		response.setMensagemSistema("E-mail inserido.");
		
		return response;		
	}

	public EmailResponse insertEmailParaEnvio(EmailRequest request) {
		EmailResponse response = new EmailResponse();

		List<Contato> listContatos = new ArrayList<Contato>();
		for (Destinatario dest : request.getDestinatarios()) {
			Contato contato = new Contato();
			contato.setEmail(dest.getEmail());
			contato.setStatus(StatusEmail.NAO_ENVIADO);
			contato.setChaveValor(ContatoUtil.loadParametros(dest.getParametros()));
			listContatos.add(contato);
		}

		listContatos = insertContatos(listContatos);

		Email email = new Email();
		email.setAssunto(request.getAssunto());
		email.setContatos(listContatos);
		email.setCorpoEmail(request.getTexto());
		email.setStatus(StatusEmail.NAO_ENVIADO);

		this.emailRepository.save(email);

		response.setMensagemSistema("E-mail inserido.");

		return response;
	}

	@Transactional
	private List<Contato> insertContatos(List<Contato> contatos) {
		List<Contato> listContatos = new ArrayList<>();
		for (Contato c : contatos) {
			listContatos.add(this.contatoRepository.save(c));
		}
		return listContatos;
	}
}
