package com.sic.enums;

public enum StatusEmail {
    ENVIADO("Enviado"),
    ERROR("Error"),
    NAO_ENVIADO("Não enviado");
	
	private StatusEmail(String descricao) {
		this.descricao = descricao;
	}

	private String descricao;

	public String getDescricao() {
		return descricao;
	}
}
