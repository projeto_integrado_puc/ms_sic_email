package com.sic.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.sic.enums.StatusEmail;

@Entity
@Table(name = "TBCONTATO")
@DynamicInsert
@DynamicUpdate
public class Contato {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String email;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private StatusEmail status;
	
	private String chaveValor;	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public StatusEmail getStatus() {
		return status;
	}

	public void setStatus(StatusEmail status) {
		this.status = status;
	}

	public String getChaveValor() {
		return chaveValor;
	}

	public void setChaveValor(String chaveValor) {
		this.chaveValor = chaveValor;
	}
}
